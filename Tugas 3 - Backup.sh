#!/bin/bash

# Menampilkan Pesan masuk,Nama komputer dan Tanggal
echo "++++ Backup Shell Script ++++"
echo
echo "+++ Run Time: $(date) @ $(hostname)"
echo
echo "Masukkan Direktory Home Anda"
echo "Contoh: /home/hendri"
read direktori

# Definisi Variable
BACKUP=$direktori
NOW=$(date +"%d-%m-%Y")

# Saat nya memulai backup
echo
echo "Melakukan backup direktori HOME ke $BACKUP/$NOW...!!!"
# Just sleep for 1 Secs
sleep 1
echo
 
if   [ -d $BACKUP/$NOW ]
then echo "> Direktori sudah ada...!!!"
     # Just Sleep for 1 Secs
     sleep 1
     echo "> Backup ulang...!!!"
     echo
     # Just Sleep for 2 Secs
     sleep 2
     rm  -r $BACKUP/$NOW/
     mkdir $BACKUP/$NOW/
     echo "++++++++++++++++++++++++++++++++++++++++++++++++++"
     tar -czvf $BACKUP/$NOW/latest.tar.gz -P $BACKUP
else echo
     echo "> Direktori tidak ditemukan...!!!"
     # Just Sleep for 1 Secs
     sleep 1
     echo "> Membuat direktori Backup...!!!"
     # Just Sleep for 1 Secs
     sleep 1
     mkdir $BACKUP/$NOW/
     echo "> Direktori sudah terbuat...!!!"
     # Just Sleep for 1 Secs
     sleep 1
     echo "> Backup...!!!"
     echo
	 # Just Sleep for 2 Secs
     sleep 2
     echo "++++++++++++++++++++++++++++++++++++++++++++++++++"
     tar -czvf $BACKUP/$NOW/latest.tar.gz -P $BACKUP
fi

# Kita sudah selesai
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo
# Just Sleep for 2 Secs
sleep 1
echo "Backup sudah ditulis di $BACKUP/$NOW/latest.tar.gz"
echo
