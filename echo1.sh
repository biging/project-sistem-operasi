#!/bin/bash

# Display welcome message, computer name and date
echo "*** Backup Shell Script ***"
echo
echo "*** Run time: $(date) @ $(hostname)"
echo "Isikan Direktory Anda"

read folder
# Define variables
BACKUP=$folder
NOW=$(date +"%d-%m-%Y")
# Let us start backup
echo "*** Dumping Direktory to $BACKUP/$NOW..."
mkdir $BACKUP/$NOW
cd $BACKUP/$NOW
tar -czvf latest.tar.gz $BACKUP

# Just sleep for 3 secs
sleep 3
# And we are done...
echo
echo "*** Backup wrote to $BACKUP/$NOW/latest.tar.gz"

