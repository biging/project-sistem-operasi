#!/bin/bash

while :
do
clear

#Tampil Menu A
echo "     Selamat Datang - $(hostname)"
echo "======================================="
echo "++++++++++    Menu Utama     ++++++++++"
echo "======================================="
echo "A. Operasi Aritmatika"
echo "B. About Me"
echo "C. Keluar"
echo "======================================="
read -p "Pilih Opsi Menu = [ A - C ] " pilihan

# mulai case in pada menu utama
	case $pilihan in
	
	a | A)
		clear
		#Tampil Menu A.1
		echo "+++++++    Menu Operasi Aritmatika     +++++++"
		echo "========================================"
		echo "1. Penjumlahan"
		echo "2. Pengurangan"
		echo "3. Perkalian"
		echo "4. Pembagian"
		echo "5. Kembali Ke Menu Utama"
		echo "========================================"
		read -p "Pilih Opsi Menu = [ 1 - 5 ] " pilihan2
		
		# mulai case in pada menu Aritmatika
		case $pilihan2 in
			1)  clear
				echo "+++++++    P E N J U M L A H A N     +++++++"
				echo
				read -p "Input Angka Pertama = " var1
				read -p "Input Angka Kedua = " var2
				hasil=$(( $var1 + $var2 ))
				echo
				echo "Hasil dari $var1 + $var2 adalah $hasil"
				echo
				read -p "Tekan [Enter] untuk melanjutkan..."
				readEnterKey
				bash TA2167200831.sh
				;;
			2) 	clear
				echo "+++++++    P E N G U R A N G A N     +++++++"
				echo
				read -p "Input Angka Pertama = " var1
				read -p "Input Angka Kedua = " var2
				hasil=$(( $var1 - $var2 ))
				echo
				echo "Hasil Dari $var1 - $var2 adalah $hasil"
				echo
				read -p "Tekan [Enter] untuk melanjutkan..."
				readEnterKey
				bash TA2167200831.sh
				;;
			3)  clear
				echo "+++++++    P E R K A L I A N     +++++++"
				echo
				read -p "Input Angka Pertama = " var1
				read -p "Input Angka Kedua = " var2
				hasil=$(( $var1 * $var2 ))
				echo
				echo "Hasil Dari $var1 x $var2 adalah $hasil"
				echo
				read -p "Tekan [Enter] untuk melanjutkan..."
				readEnterKey
				bash TA2167200831.sh
				;;
			4)  clear
				echo "+++++++    P E M B A G I A N     +++++++"
				echo
				read -p "Input Angka Pertama = " var1
				read -p "Input Angka Kedua = " var2
				hasil=$(( $var1 / $var2 ))
				echo
				echo "Hasil Dari $var1 : $var2 adalah $hasil"
				echo
				read -p "Tekan [Enter] untuk melanjutkan..."
				readEnterKey
				bash TA2167200831.sh
				;;
			5)  bash TA2167200831.sh
				;;
			*)
				echo
				echo "Error: Pilihan Anda Salah..."
				echo
				read -p "Tekan [Enter] Untuk Melanjutkan........"
				readEnterKey
				bash TA2167200831.sh
				;;
		esac
			echo
			read -p "Tekan [Enter] untuk melanjutkan..."
			readEnterKey
			bash TA2167200831.sh
	;;

	b | B)
		clear
		#Tampil Menu A.2
		echo "++++++   Menu About Me   +++++"
		echo "========================================"
		echo "1. Nama, NIM, Home Dir"
		echo "2. Username@ Hostname"
		echo "3. Kembali Ke Menu Utama"
		echo "========================================"
		read -p "Pilih Opsi Menu = [ 1 - 3 ] " pilihan2
		# mulai case in pada menu Aritmatika
		case $pilihan2 in
			1)  clear
				echo "+++++++    S A L A M  K E N A L     +++++++"
				echo
				echo "Nama		: Hendri Setiawan"
				echo "NIM		: 2167200831"
				echo "Home Dir	: /home/hendri/"
				echo
				read -p "Tekan [Enter] untuk melanjutkan..."
				readEnterKey
				bash TA2167200831.sh
				;;
			2) 	clear
				echo "+++++++    I N T R O D U C T I O N    +++++++"
				echo
				echo "Hallo Selamat Datang - $(hostname)"
				echo "Ini adalah program sederhana yang sudah aku buat"
				echo "Semoga dapat membantu pekerjaanmu"
				echo "Thanks for all!"
				echo
				read -p "Tekan [Enter] untuk melanjutkan..."
				readEnterKey
				bash TA2167200831.sh
				;;
			3)  bash TA2167200831.sh
				;;
			*)
				echo
				echo "Error: Pilihan Anda Salah..."
				echo
				read -p "Tekan [Enter] Untuk Melanjutkan........"
				bash TA2167200831.sh
				readEnterKey
				;;
		esac
			echo
			read -p "Tekan [Enter] untuk melanjutkan..."
			readEnterKey
	;;

	c | C)
		echo
		echo "Sampai Jumpa Lagi!"
		exit 0
		;;
	*)	
		echo
		echo "Error: Pilihan Anda Salah..."
		echo
		read -p "Tekan [Enter] Untuk Melanjutkan........"
		readEnterKey
		;;
	esac

done 