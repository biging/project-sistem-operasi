#!/bin/bash
# Display welcome message, computer name and date
echo "*** Backup Shell Script ***"
echo
echo "*** Run Time: $(date) @ $(hostname)"
echo
# Define variables
BACKUP="/home/hendri"
NOW=$(date +"%d-%m-%Y")
# Let us start backup
echo "*** Dumping MySQL Database to $BACKUP/$NOW..."
if [ $BACKUP/$NOW/latest.tar.gz ]
then
rm -r $BACKUP/$NOW
mkdir $BACKUP/$NOW
else 
mkdir $BACKUP/$NOW
fi
tar -czvf $NOW/latest.tar.gz -P $BACKUP/
# Just sleep for 3 second
sleep 3
# And we are done...
echo
echo "*** Backup wrote to $BACKUP/$NOW/latest.tar.gz"
