@ECHO OFF
title HIDE/UNHIDE
:MENU
cls
echo Silahkan pilih...
echo [==============================================]
echo [(A) Hide _____ (B) Unhide _____ (S) SuperUNHIDE]
echo [==============================================]
echo .
echo (L) Daftar Folder Tersembunyi
echo (F) Daftar File Tersembunyi
echo (X) Exit
color 71
set /p "cho=>"
if %cho%==A goto HIDE		//untuk menyembunyikan file/Folder yang dipilih
if %cho%==a goto HIDE
if %cho%==B goto UNHIDE		//untuk menampilkan file/Folder yang dipilih
if %cho%==b goto UNHIDE
if %cho%==X goto END		//untuk keluar dari program yang dibuat
if %cho%==x goto END
if %cho%==S goto SUPER		//untuk menampilkan semua file/Folder
if %cho%==s goto SUPER
if %cho%==L goto LIST		//untuk menampilkan Folder yang tersembunyi
if %cho%==l goto LIST
if %cho%==F goto FILE		//untuk menampilkan file yang tersembunyi
if %cho%==f goto FILE
goto FALSE

:LIST
cls
echo [Daftar Folder Tersembunyi]
echo ===========================
DIR /adh /b		//menampilkan folder yang tersembunyi (hanya nama folder saja)
pause
goto MENU

:FILE
cls
echo [Daftar File Tersembunyi]
echo ===========================
DIR /a-dh /b		//menampilkan bukan folder yang tersembunyi (hanya nama folder saja)
pause
goto MENU

:SUPER
cls
echo ===ANDA AKAN MENAMPILKAN SEMUA FILE!!!!!===
echo ===Apakah anda ingin melanjutkan??? (Y) ===
color 9c
set/p "sup=>"
if %sup%==Y goto SUPER1
if %sup%==y goto SUPER1
goto END

:SUPER1
attrib /d /s -s -h	//meng-unhide/mengurangi atribut hidel pada semua file maupun folder
goto MENU

:UNHIDE
cls
echo UNHIDE
ECHO "Nama file/Folder"
set /p "name=>"
if NOT EXIST %name% goto NOT
attrib -s -h %name%
cls
echo File/Folder telah di tampilkan
timeout 5 >nul
goto MENU

:HIDE
cls
echo HIDE
ECHO "Nama file/Folder"
set /p "name=>"
if NOT EXIST %name% goto NOT
attrib +s +h %name%
cls
echo File/Folder telah di sembunyikan
timeout 5 >nul
goto MENU

:FALSE
cls
echo pilihan anda salah!!!
color 9c
timeout 5 >nul
goto MENU

:NOT
cls
color 9c
echo "File tidak ada"
pause >nul
goto MENU

:END
Exit